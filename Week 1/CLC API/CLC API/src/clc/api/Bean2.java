/* Stephan Moncavage
 * CST235
 * 14 August 2018
 * This is my own work. 
 * This is code for Question 1 as part of CLC Mini Project 1 in CST 235 Purple Group Project
 * How can configuration settings of a bean be made persistent? Provide code examples.  
 * Substantiate your answer with code examples and screenshots of execution. Seek instructor guidance as needed.
 */
package clc.api;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Bean2 implements Serializable{

    @Id//primary key
    @Column(name = "ID")
    private Long id;
    @Column(name = "Title")
    private String title;
    @Column (name = "Price")
    private float price;
    
    public static void main(String[]args){
       Bean2 newbean = new Bean2();
       newbean.setId(Long.MIN_VALUE);
       newbean.setTitle("Hammy");
       newbean.setPrice((float) 42.83);
        System.out.println(newbean);
    }
    
    public Bean2(){
         
    }
    
    public void setId(Long h){
        this.id = h;
    }
       
    public long getId(){
        return id;
    }
    
    public void setTitle(String string){
        this.title = string;
    }  
    
    public String getTitle(){
        return title;
    }
    
    public void setPrice(float j){
        this.price = j;
    }
    
    public float getPrice(){
        return price;
    }
    
}
