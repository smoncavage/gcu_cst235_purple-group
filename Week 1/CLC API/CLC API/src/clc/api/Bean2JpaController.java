/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clc.api;

import clc.api.exceptions.NonexistentEntityException;
import clc.api.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author monca
 */
public class Bean2JpaController implements Serializable {

    public Bean2JpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Bean2 bean2) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(bean2);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findBean2(bean2.getId()) != null) {
                throw new PreexistingEntityException("Bean2 " + bean2 + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Bean2 bean2) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            bean2 = em.merge(bean2);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                long id = bean2.getId();
                if (findBean2(id) == null) {
                    throw new NonexistentEntityException("The bean2 with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Bean2 bean2;
            try {
                bean2 = em.getReference(Bean2.class, id);
                bean2.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The bean2 with id " + id + " no longer exists.", enfe);
            }
            em.remove(bean2);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Bean2> findBean2Entities() {
        return findBean2Entities(true, -1, -1);
    }

    public List<Bean2> findBean2Entities(int maxResults, int firstResult) {
        return findBean2Entities(false, maxResults, firstResult);
    }

    private List<Bean2> findBean2Entities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Bean2.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Bean2 findBean2(long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Bean2.class, id);
        } finally {
            em.close();
        }
    }

    public int getBean2Count() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Bean2> rt = cq.from(Bean2.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
