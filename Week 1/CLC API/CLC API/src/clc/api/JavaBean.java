//Joey Mancini
//CST235
//8-17-18
//This is my own work.

package clc.api;

//Imports
import java.io.*;

public class JavaBean implements Serializable
{
    //Variables
    private String beanName;
    private int beanID;
    private int backupID;
    private int reversedBeanID;

    //No-argument constructor
    public JavaBean() { }

    //setBeanName
    public void setBeanName(String beanName)
    {
        this.beanName = beanName;
    }

    //getBeanName
    public String getBeanName()
    {
        return ("The bean name is " + this.beanName + ".");
    }

    //setBeanID
    public void setBeanID(int beanID)
    {
        this.beanID = beanID;
        this.backupID = beanID;
    }

    //getBeanID
    public String getBeanID()
    {
        return ("The bean ID is " + this.beanID + ".");
    }

    //setReversedBeadID
    public void setReversedBeanID()
    {
        //Reverse the ID
        while (beanID > 0)
        {
            this.reversedBeanID += (this.beanID % 10);
            this.beanID /= 10;

            //Make sure we don't add an extra zero at the end
            if (beanID > 0)
                this.reversedBeanID *= 10;
        }

        //Update the original ID
        this.beanID = this.backupID;
    }

    //getReversedBeanID
    public String getReversedBeanID()
    {
        return ("The reversed bean ID is " + this.reversedBeanID + ".");
    }

}