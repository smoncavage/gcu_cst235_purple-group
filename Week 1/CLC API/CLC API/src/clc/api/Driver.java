//Joey Mancini
//CST235
//8-17-18
//This is my own work.

package clc.api;

//Imports
import java.beans.*;

public class Driver
{
    public static void main(String[] args) throws Exception
    {
        //Variables
        BeanInfo bean = Introspector.getBeanInfo(JavaBean.class, Object.class);
        PropertyDescriptor[] propDesc = bean.getPropertyDescriptors();

        //For-loop to print all the porperties of the bean
        System.out.println("***Property Descriptor Variables***\n");
        for (PropertyDescriptor pd : propDesc)
        {
            System.out.println(pd.getName());
        }
        
        //Decapitalizing tests
        System.out.println("\n\n***Decapitalize Tests***\n");
        System.out.println(Introspector.decapitalize("Capitalized only the first letter") );
        System.out.println(Introspector.decapitalize("Capitalized All First Letters") );
        System.out.println(Introspector.decapitalize("ALL LETTERS ARE CAPITALIZED") );

        //Create a bean
        System.out.println("\n\n***Creating a JavaBean Bean***\n");
        JavaBean theBean = new JavaBean();
        theBean.setBeanName("The Bean");
        theBean.setBeanID(2345);
        theBean.setReversedBeanID();

        //Print it
        System.out.println(theBean.getBeanName()) ;
        System.out.println(theBean.getBeanID() );
        System.out.println(theBean.getReversedBeanID() );

    }
}
