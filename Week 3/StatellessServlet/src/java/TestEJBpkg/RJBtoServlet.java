/* Stephan Moncavage
 * CST235
 * 23 August 2018
 * This is my own work. Based upon the below information
 * The simplegreeting CDI Example. (n.d.). Retrieved from https://docs.oracle.com/javaee/7/tutorial/cdi-basicexamples001.htm#GJBJU
 */
package TestEJBpkg;

import java.io.*;
import javax.ejb.EJB;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import util.HTMLFilter;

@WebServlet(name="Servlet", urlPatterns={"/servlet"})
public class RJBtoServlet extends HttpServlet{
    
    private RJBtoServlet rjbs;
    @Override
    public void service(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();

        try {

            out.println("<HTML> <HEAD> <TITLE> Servlet Output </TITLE> </HEAD> <BODY BGCOLOR=white>");
            out.println("<CENTER> <FONT size=+1> Servlet2Stateless:: Please enter your name </FONT> </CENTER> <p> ");
            out.println("<form method=\"POST\">");
            out.println("<TABLE>");
            out.println("<tr><td>Name: </td>");
            out.println("<td><input type=\"text\" name=\"name\"> </td>");
            out.println("</tr><tr><td></td>");
            out.println("<td><input type=\"submit\" name=\"sub\"> </td>");
            out.println("</tr>");
            out.println("</TABLE>");
            out.println("</form>");
            String val = req.getParameter("name");
            
            if ((val != null) && (val.trim().length() > 0)) {
                out.println("<FONT size=+1 color=red> Greeting from StatelessSessionBean: </FONT>"
                            + HTMLFilter.filter(RJBSession.hello(val)) + "<br>");
            }
            out.println("</BODY> </HTML> ");

        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("webclient servlet test failed");
            throw new ServletException(ex);
        }
    }
}
