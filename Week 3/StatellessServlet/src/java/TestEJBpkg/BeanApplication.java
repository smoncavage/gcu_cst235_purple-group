/* Stephan Moncavage
 * CST235
 * 23 August 2018
 * This is my own work. 
 */
package TestEJBpkg;


import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class BeanApplication extends Application {
    public static void main(String[] args) {
        launch(args);       
    }
    @Override
    public void start(Stage primaryStage) {
        
        Button btn = new Button();
        btn.setText("Guess Who...");
        
        GridPane root = new GridPane();
        root.setAlignment(Pos.CENTER);
        root.setPadding(new Insets(0,0,0,0));
        root.setHgap(5);
        root.setVgap(5);
        
        Scene scene = new Scene(root, 300, 250);
        root.add((btn), 0, 0);
        
        btn.setOnAction((ActionEvent event) -> {
            RegJavaBean.setName("Jason");
            Text text = new Text(RegJavaBean.getName());
            System.out.println(RegJavaBean.getName());
            root.add((text), 0, 3);
            
            primaryStage.setTitle("Bean Application");
            primaryStage.setScene(scene);
            primaryStage.show();
        });
        
        primaryStage.setTitle("Bean Application");
        primaryStage.setScene(scene);
        primaryStage.show();
    }  
}