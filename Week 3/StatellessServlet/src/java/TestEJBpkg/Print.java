/* Stephan Moncavage
 * CST235
 * 23 August 2018
 * This is my own work. Based upon the below information
 * The simplegreeting CDI Example. (n.d.). Retrieved from https://docs.oracle.com/javaee/7/tutorial/cdi-basicexamples001.htm#GJBJU
 */
package TestEJBpkg;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped
public class Print {

    @Inject
    RegJavaBean sessbean;
    
    private String name;
    private String welcome;

    public void setWelcome() {
        this.welcome = RJBSession.hello("Hello "+ name + " welcome to the faces inject servlet.");
    }

    public String getWelcome() {
        return welcome;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
