/* Stephan Moncavage
 * CST235
 * 23 August 2018
 * This is my own work. 
 */
package TestEJBpkg;

import java.util.List;
import javax.ejb.Local;

@Local
public interface EntityFacadeLocal {

    void create(Entity entity);

    void edit(Entity entity);

    void remove(Entity entity);

    Entity find(Object id);

    List<Entity> findAll();

    List<Entity> findRange(int[] range);

    int count();
    
}
