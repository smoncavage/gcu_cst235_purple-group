/* Stephan Moncavage
 * CST235
 * 23 August 2018
 * This is my own work. Based upon the below information
 * The simplegreeting CDI Example. (n.d.). Retrieved from https://docs.oracle.com/javaee/7/tutorial/cdi-basicexamples001.htm#GJBJU
 */
package TestEJBpkg;

import javax.ejb.Stateless;

@Stateless
public class RJBSession {

    public static String hello(String name) {
        return "Hello, " + name + " Would you like a drink?";
    }

}
