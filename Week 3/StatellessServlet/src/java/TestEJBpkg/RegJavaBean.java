/* Stephan Moncavage
 * CST235
 * 23 August 2018
 * This is my own work. Based upon the below information
 * The simplegreeting CDI Example. (n.d.). Retrieved from https://docs.oracle.com/javaee/7/tutorial/cdi-basicexamples001.htm#GJBJU
 */
package TestEJBpkg;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;

@RequestScoped
@Local
@Remote

public class RegJavaBean implements Serializable{
        
        static String cola;
	static String name;
	double drinkPrice;    
                
        public static void setCola(){
            RegJavaBean.cola = cola;
        }
   
        public static String getCols (){
            return cola;
        }
        public static void setName(String name){
            RegJavaBean.name = name;
        }
        
        public static String getName (){
            return name;
        }
        
}
