/* Stephan Moncavage
 * CST235
 * 23 August 2018
 * This is my own work. 
 */
package TestEJBpkg;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author monca
 */
@Stateless
@Local
public class EntityFacade extends AbstractFacade<Entity> implements EntityFacadeLocal {

    @PersistenceContext(unitName = "StatellessServletPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EntityFacade() {
        super(Entity.class);
    }
    
}
