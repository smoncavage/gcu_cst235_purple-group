<%-- 
    Document   : newjsp
    Created on : Aug 25, 2018, 12:47:32 PM
    Author     : monca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:useBean id='date'  class='java.util.Date'/>
            <jsp:useBean id='name'  class='TestEJBpkg.RegJavaBean'>
                <jsp:setProperty name='regjavabean' property='name' value='Jeffery'/>
                <jsp:setProperty name='regjavabean' property='cola' value='rootbeer'/>
                
            </jsp:useBean>
        <jsp:text>
            Welcome to direct injection servlet.
        </jsp:text>
    </body>
</html>
